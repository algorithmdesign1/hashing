/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.algorithmdesignhashing;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class TestHashing {
    public static void main(String[] args) {      
        
        Hashing hash = new Hashing(11);       
        
        hash.put(11, "Alice");      // 0
        hash.put(25, "Laville");    // 3
        hash.put(7, "Krixi");       // 7
        hash.put(55, "Annette");    // 0 >> replaced 11 Alice
        hash.put(43, "Paine");      // 10
        hash.remove(43);            // remove 43 Paine
        hash.getAll();
        
        System.out.println("After remove all data in table");
        hash.removeAll();
        hash.getAll();

    }
}
