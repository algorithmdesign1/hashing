/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.algorithmdesignhashing;

import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class Hashing {

    int size;
    int key;
    String value;
    ArrayList<String> table = new ArrayList<String>();

    public Hashing(int size) {
        this.size = size;

        setNullDefaultTable(size);
    }

    // Set null value in default table 
    public void setNullDefaultTable(int size) {
        for (int i = 0; i < size; i++) {
            table.add(i, null);
        }
    }

    public int getHash(int key) {
        int h = key % size;
        return h;
    }

    // insert key, value (passed getHash) into ArrayList
    public void put(int key, String value) {
        this.key = key;
        this.value = value;

        int h = getHash(key);
        table.set(h, value);
    }

    // Search value from key
    public String get(int key) {
        int h = getHash(key);
        return table.get(h);
    }

    public void getAll() {
        for (int i = 0; i < size; i++) {
            System.out.println("[ Key:" + i + ", Value:" + table.get(i) + " ]");
        }
    }

    public void remove(int key) {
        int h = getHash(key);
        table.set(h, null);
    }

    public void removeAll() {
        for (int i = 0; i < size; i++) {
            table.set(i, null);
        }
    }
}
